# Eff You Mongo DB for making this difficult lol

# import necessary libraries from creds
import datetime
from creds import MONGODB_URI_ATLAS, MONGODB_URI_LOCAL
import json
from pymongo import MongoClient
import requests

# set global vars
MONGODB_URI = MONGODB_URI_ATLAS
DATABASE = "Myujen"
COLLECTION = "Emails"
ADD_EMAIL = True
EMAIL_API_URL = "http://localhost:3000/api/post-email"

# name of file for this version of backup
todays_date = datetime.datetime.now().strftime('%Y-%m-%d')
file_name = f'backups/{DATABASE}-{COLLECTION}-{todays_date}.json'

# create client with MONGODB_URI
client = MongoClient(MONGODB_URI)
# get the database
db = client[DATABASE]
# find all users in Users collection
data = db[COLLECTION].find()

# initialize count and array
count = 0
j = []

# loop through users and convert ObjectId to string
for d in data:
  objId = d["_id"]
  d["_id"] = str(objId)
  j.append(d)
  count+=1

  # Optional: Add Emails to New Database via Localhost API Call
  if ADD_EMAIL:
    print("Email: " + d["Email"])
    payload = json.dumps({
      "email": d["Email"]
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", EMAIL_API_URL, headers=headers, data=payload)
    print("Status Code: " + str(response.status_code))

# write array to JSON file
with open(file_name, 'w') as outfile:
  json.dump(list(j), outfile)

# print confirmation that backup is complete
print("User Backup Complete! Users: " + str(count))