# This doesn't work btw LOOOOL

import json
import requests

# Set global vars
API_URL = "http://localhost:3000/api/world/recreate"
FILE_NAME="backups/Myujen-Worlds-2023-10-07.json"

# return empty string if key:value does not exist in the dictionary
def check_dict_keys(d, key):
  if key in d:
    return d[key]
  else:
    return ""

# Loop over worlds json and re-create worlds
with open(FILE_NAME) as json_file:
    data = json.load(json_file)
    for d in data:
      print(d)
      payload = json.dumps({
        "imageUrl": check_dict_keys(d, "ImageUrl"),
        "prompt": check_dict_keys(d, "Prompt"), 
        "pusherChannel": check_dict_keys(d, "PusherChannel"),
        "pusherEvent": check_dict_keys(d, "PusherEvent"),
        "resizedUrl": check_dict_keys(d, "ResizedUrl"),
        "skyboxId": check_dict_keys(d, "SkyboxId"),
        "skyboxStyle": check_dict_keys(d, "SkyboxStyle"),
        "status": check_dict_keys(d, "Status"),
        "thumbUrl": check_dict_keys(d, "ThumbUrl"),
        "title": check_dict_keys(d, "Title"),
        "userEmail": check_dict_keys(d, "UserEmail")
      })
      headers = {
        'Content-Type': 'application/json'
      }

      response = requests.request("POST", API_URL, headers=headers, data=payload)
      print(f"==Status Code: {str(response.status_code)}==" )
