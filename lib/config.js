
const FEATURES_COMPLETE = [
  {"feature": "Create Worlds from UI", "date": "April 2023"},
  {"feature": "Remix Worlds from UI", "date": "April 2023"},
]

const FEATURES_UPCOMING = [
  {"feature": "Upload Images into Worlds", "priority": "High"},
  {"feature": "Upload Music into Worlds", "priority": "High"},
  {"feature": "Upload Text into Worlds", "priority": "High"},
  {"feature": "Volume Slider in  Worlds", "priority": "High"},
  {"feature": "Linking Worlds Together (Portals)", "priority": "Medium"},
  {"feature": "Upload 3D Models into Worlds", "priority": "Medium"},
  {"feature": "User Profiles", "priority": "Medium"},
  {"feature": "Avatar Generator", "priority": "Low"},
  {"feature": "Multiplayer", "priority": "Low"},
  {"feature": "Ranking/Voting Words", "priority": "Low"},
  {"feature": "Text to CSV Converter", "priority": "Low"},
]

const FORM_URL = "https://forms.gle/L7gzkJAT3B23zbc36"

const PAGES = [
  {"name": "Create", "desc": "Generate a Myujen world", "link": "/create"},
  {"name": "Explore", "desc": "View public Myujen worlds", "link": "/explore"},
  {"name": "Feedback", "desc": "Submit feedback and request features for the next release", "link": `${FORM_URL}`},
  {"name": "Profile", "desc": "Check your current email and logout", "link": "/profile"},
  {"name": "Roadmap", "desc": "List of upcoming and prioritized features", "link": "/roadmap"},
  {"name": "Subscribe", "desc": "Support Myujen's development and get exclusive rewards!", "link": "https://ko-fi.com/atemosta/tiers#"},
  {"name": "Twitter", "desc": "Get live updates on feature releases", "link": "https://twitter.com/OscoXR"},
]

const PUSHER_CLUSTER = "mt1"

const PUSHER_KEY = "a6a7b7662238ce4494d5"

const SITE_ABOUT = "Create virtual reality scenes in a couple of words, like this one:"

const SITE_NAME = "Myujen"

const SITE_TITLE = "Myujen"

const STYLES = [
  {
      "id": 5,
      "name": "Digital Painting",
      "max-char": "420",
      "image": null,
      "sort_order": 1
  },
  {
      "id": 9,
      "name": "Realistic",
      "max-char": "360",
      "image": null,
      "sort_order": 2
  },
  {
      "id": 2,
      "name": "Fantasy Lands",
      "max-char": "350",
      "image": "https://blockade-platform-production.s3.amazonaws.com/images/skybox/imG0GHHOlErLJzWE90hxK5cpmyoDofV0AVtDXAHa.png",
      "sort_order": 4
  },
  {
      "id": 10,
      "name": "SciFi",
      "max-char": "360",
      "image": null,
      "sort_order": 6
  },
  {
      "id": 6,
      "name": "Scenic",
      "max-char": "420",
      "image": null,
      "sort_order": 8
  },
  {
      "id": 11,
      "name": "Dreamlike",
      "max-char": "400",
      "image": null,
      "sort_order": 9
  },
  {
      "id": 17,
      "name": "Oil Painting",
      "max-char": "320",
      "image": null,
      "sort_order": 10
  },
  {
      "id": 25,
      "name": "Manga (Testing)",
      "max-char": "420",
      "image": null,
      "sort_order": 11
  },
  {
      "id": 16,
      "name": "Sky",
      "max-char": "350",
      "image": null,
      "sort_order": 12
  },
  {
      "id": 15,
      "name": "Interior Views",
      "max-char": "400",
      "image": null,
      "sort_order": 13
  },
  {
      "id": 4,
      "name": "Surreal Style",
      "max-char": "340",
      "image": null,
      "sort_order": 14
  },
  {
      "id": 7,
      "name": "Nebula",
      "max-char": "420",
      "image": null,
      "sort_order": 16
  },
  {
      "id": 24,
      "name": "Pen & Ink (testing)",
      "max-char": "300",
      "image": null,
      "sort_order": 17
  },
  {
      "id": 23,
      "name": "Technical (testing)",
      "max-char": "300",
      "image": null,
      "sort_order": 18
  },
  {
      "id": 19,
      "name": "Low Poly (Experimental)",
      "max-char": "460",
      "image": null,
      "sort_order": 19
  },
  {
      "id": 26,
      "name": "Interior Archviz (testing)",
      "max-char": "370",
      "image": null,
      "sort_order": 20
  },
  {
      "id": 18,
      "name": "Infrared (experimental nature scenes)",
      "max-char": "420",
      "image": "https://blockade-platform-production.s3.amazonaws.com/images/skybox/cMcfRvpPVKz3cZM5cWbKYICcyxvfPTeKzn5Yn0hY.jpg",
      "sort_order": 21
  },
]

const VR_SAMPLE_ID = "6451564519fa2bc25ac038ba"

const VR_SAMPLE_PROMPT = "View from inside a Treehouse In a dense Jungle full of lush foliage"

const VR_URL = "https://myujen-aframe.vercel.app/index.html?id="

export {
  FEATURES_COMPLETE,
  FEATURES_UPCOMING,
  FORM_URL,
  PAGES,
  PUSHER_CLUSTER,
  PUSHER_KEY,
  SITE_ABOUT,
  SITE_NAME,
  SITE_TITLE,
  STYLES,
  VR_SAMPLE_ID,
  VR_SAMPLE_PROMPT,
  VR_URL
}
