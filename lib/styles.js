const styles = [
  {
      "id": 5,
      "name": "Digital Painting",
      "max-char": "420",
      "image": null,
      "sort_order": 1
  },
  {
      "id": 9,
      "name": "Realistic",
      "max-char": "360",
      "image": null,
      "sort_order": 2
  },
  {
      "id": 3,
      "name": "Anime Art Style",
      "max-char": "425",
      "image": "https://blockade-platform-production.s3.amazonaws.com/images/skybox/Ki2I4oxMHoZRiKzCCGJD7TbTjgQ69N7QICTzb2zx.png",
      "sort_order": 3
  },
  {
      "id": 2,
      "name": "Fantasy Lands",
      "max-char": "350",
      "image": "https://blockade-platform-production.s3.amazonaws.com/images/skybox/imG0GHHOlErLJzWE90hxK5cpmyoDofV0AVtDXAHa.png",
      "sort_order": 4
  },
  {
      "id": 13,
      "name": "Advanced (no style)",
      "max-char": "540",
      "image": null,
      "sort_order": 5
  },
  {
      "id": 10,
      "name": "SciFi",
      "max-char": "360",
      "image": null,
      "sort_order": 6
  },
  {
      "id": 20,
      "name": "Modern Computer Animation",
      "max-char": "480",
      "image": null,
      "sort_order": 7
  },
  {
      "id": 6,
      "name": "Scenic",
      "max-char": "420",
      "image": null,
      "sort_order": 8
  },
  {
      "id": 11,
      "name": "Dreamlike",
      "max-char": "400",
      "image": null,
      "sort_order": 9
  },
  {
      "id": 17,
      "name": "Oil Painting",
      "max-char": "320",
      "image": null,
      "sort_order": 10
  },
  {
      "id": 25,
      "name": "Manga (Testing)",
      "max-char": "420",
      "image": null,
      "sort_order": 11
  },
  {
      "id": 16,
      "name": "Sky",
      "max-char": "350",
      "image": null,
      "sort_order": 12
  },
  {
      "id": 15,
      "name": "Interior Views",
      "max-char": "400",
      "image": null,
      "sort_order": 13
  },
  {
      "id": 4,
      "name": "Surreal Style",
      "max-char": "340",
      "image": null,
      "sort_order": 14
  },
  {
      "id": 22,
      "name": "Watercolor (testing)",
      "max-char": "375",
      "image": null,
      "sort_order": 15
  },
  {
      "id": 7,
      "name": "Nebula",
      "max-char": "420",
      "image": null,
      "sort_order": 16
  },
  {
      "id": 24,
      "name": "Pen & Ink (testing)",
      "max-char": "300",
      "image": null,
      "sort_order": 17
  },
  {
      "id": 23,
      "name": "Technical (testing)",
      "max-char": "300",
      "image": null,
      "sort_order": 18
  },
  {
      "id": 19,
      "name": "Low Poly (Experimental)",
      "max-char": "460",
      "image": null,
      "sort_order": 19
  },
  {
      "id": 26,
      "name": "Interior Archviz (testing)",
      "max-char": "370",
      "image": null,
      "sort_order": 20
  },
  {
      "id": 18,
      "name": "Infrared (experimental nature scenes)",
      "max-char": "420",
      "image": "https://blockade-platform-production.s3.amazonaws.com/images/skybox/cMcfRvpPVKz3cZM5cWbKYICcyxvfPTeKzn5Yn0hY.jpg",
      "sort_order": 21
  },
  {
      "id": 28,
      "name": "testing",
      "max-char": "260",
      "image": null,
      "sort_order": 22
  }
]

export {
  styles
}