// next.config.js
module.exports = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'blockade-platform-production.s3.amazonaws.com',
        port: '',
        pathname: '/**',
      },
    ],
  },
}
