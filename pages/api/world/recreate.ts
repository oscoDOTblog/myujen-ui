// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../../lib/mongodb'


// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Parse Request Body
  const { body } = req
  const { 
    imageUrl,
    prompt,
    pusherChannel,
    pusherEvent,
    resizedUrl,
    skyboxId,
    skyboxStyle,
    status,
    thumbUrl,
    title, 
    userEmail
  } = body
  // Print Out Current Payload
  console.log(body)

  // Connect to Database
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");

  // Create New Document in Database
  let newDocument = {
    "ImageUrl": imageUrl,
    "Prompt": prompt,
    "PusherChannel": pusherChannel,
    "PusherEvent":     pusherEvent,
    "ResizedUrl": resizedUrl,
    "SkyboxId": skyboxId,
    "SkyboxStyle": skyboxStyle,
    "Status": status,
    "ThumbUrl": thumbUrl,
    "Title": title,
    "UserEmail": userEmail   
  }
  let result = await collection.insertOne(newDocument);
  res.status(200).json(result)
}