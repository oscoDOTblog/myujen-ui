// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../../lib/mongodb'
import { BlockadeLabsSdk } from '@blockadelabs/sdk';
if (!process.env.BLOCKADE_LABS_API_KEY) {
  throw new Error('Invalid/Missing environment variable: "BLOCKADE_LABS_API_KEY"')
}

// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Parse Request Body
  const { body } = req
  const { 
    prompt,
    skyboxStyleId,
    skyboxStyleName,
    title, 
    userEmail
  } = body
  // Print Out Current Payload
  console.log(body)

  // Connect to Blockade Labs SDK
  const API_KEY = process.env.BLOCKADE_LABS_API_KEY
  const sdk = new BlockadeLabsSdk({
    api_key: API_KEY, // REQUIRED
  });

  // Generate Skybox Based on Request Body
  console.log("prompt: "+ prompt)
  const generation = await sdk.generateSkybox({
    prompt: prompt, // Required
    skybox_style_id: parseInt(skyboxStyleId), // Required
    // webhook_url: 'YOUR_WEBHOOK_URL', // Optional
  });

  // Parse Values from Generation
  const skyboxId = generation.id 
  const pusher_channel = generation.pusher_channel
  const pusher_event = generation.pusher_event

  // Connect to Database
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");

  // Create New Document in Database
  let newDocument = {
    "ImageUrl": "TBD",
    "Prompt": prompt,
    "PusherChannel": pusher_channel,
    "PusherEvent": pusher_event,
    "SkyboxId": skyboxId,
    "SkyboxStyle": skyboxStyleName,
    "Status": "pending",
    "ThumbUrl": "TBD",
    "Title": title,
    "UserEmail": userEmail   
  }
  let result = await collection.insertOne(newDocument);
  let result2 = {
    acknowledged: result.acknowledged,
    insertedId: result.insertedId,
    pusherChannel: pusher_channel,
    pusherEvent: pusher_event,
  }
  res.status(200).json(result2)
}