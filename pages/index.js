import Button from '@mui/material/Button';
import Head from 'next/head'
import Image from 'next/image';
import Link from 'next/link';
import Layout, { siteTitle } from '../components/layout'
// import { useContext } from 'react';
// import { UserContext } from '../lib/UserContext'
import TextField from '@mui/material/TextField';
import utilStyles from '../styles/utils.module.css'
import { PAGES, SITE_ABOUT, SITE_TITLE } from '../lib/config';
import { VR_SAMPLE_ID, VR_SAMPLE_PROMPT, VR_URL } from '../lib/config';


export default function Home({}) {
  // Allow this component to access our user state
  // const [user] = useContext(UserContext);

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        {/* <h3>Welcome to <i>{SITE_TITLE}</i> by <i><Link href="https://atemosta.com">Atemosta</Link></i></h3> */}
        <center><h3>Welcome to Myujen, a platform for creating <br/><i>Virtual Reality Visual Novels</i></h3></center>
        <h4>{SITE_ABOUT}</h4>
        <center>
          <Image
            src="https://blockade-platform-production.s3.amazonaws.com/thumbs/imagine/thumb_beautiful_vr_anime_illustration_view_view_inside_treehouse_dense__4ad09b2596e5345f__3023922_4ad.jpg?ver=1"
            width={500}
            height={250}
            alt="Picture of the author"
          />
          <br/>
          <TextField
              disabled
              id="filled-disabled"
              label="Prompt"
              defaultValue={VR_SAMPLE_PROMPT}
              variant="filled"
              fullWidth
            />
          <br/>
          <br/>
          <Link href={`${VR_URL}${VR_SAMPLE_ID}`}><Button variant="contained">Enter World</Button></Link>
        </center>
        {PAGES.map(({name, desc, link}) => (
          <p key={name}>
            <Link href={link}>{name}</Link> - {desc}
          </p>
        ))}
      </section>
    </Layout>
  )
}